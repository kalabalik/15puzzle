var idList = ["t1", "t2", "t3", "t4", "t5", "t6", "t7", "t8", "t9", "t10", "t11", "t12", "t13", "t14", "t15", "t0"];

function init() {
    var mq = window.matchMedia("(min-width: 600px) and (min-height: 400px)");
    mq.addListener(mqListener);
    var button = document.querySelector(".t16");
    button.addEventListener("click", buttonClickListener);
    //button.addEventListener("mousedown", buttonDownListener(button));
    //button.addEventListener("mouseup", buttonUpListener(button));
    var body = document.querySelector("body");
    body.addEventListener("mouseup", buttonUpListener(body));
    var tileSize = parseInt(getComputedStyle(document.body).getPropertyValue("--tilesize"), 10);
    for (var column = 0; column < 4; column++) {
        for (var row = 0; row < 4; row++) {
            var div = document.querySelector("." + idList[row * 4 + column]);
            div.style.top = row * tileSize + "px";
            div.style.left = column * tileSize + "px";
            if (idList[row * 4 + column] !== "t0") {
                div.addEventListener("click", clickListener);
            }
        }
    }
    for (i = 0; i < 10000; i++) {
        var fakeClick = parseInt(Math.random() * 15);
        swapTiles(idList[fakeClick]);
    }
}

function mqListener(mq) {
    var factor = 1;
    if (mq.matches) {
        factor = 2;
        // workaround because media query callback gets called twice
        for (var i = 0; i < 16; i++) {
            var div = document.querySelector("." + idList[i]);
            if (parseInt(div.style.top) * factor > 300) {
                return;
            }
        }
    } else {
        factor = 0.5;
        // workaround because media query callback gets called twice
        for (var i = 0; i < 16; i++) {
            var div = document.querySelector("." + idList[i]);
            if (parseInt(div.style.top) != 0 && parseInt(div.style.top) * factor < 50) {
                return;
            }
        }
    }
    for (var i = 0; i < 16; i++) {
        var div = document.querySelector("." + idList[i]);
        div.style.top = parseInt(div.style.top) * factor + "px";
        div.style.left = parseInt(div.style.left) * factor + "px";
    }
}

function buttonDownListener(elem) {
    elem.style.color = "darkslategrey";
}

function buttonUpListener(elem) {
    elem.style.color = "darkorange";
}

function buttonClickListener() {
    init();
}

function clickListener(e) {
    var selectedTileClass = e.target.classList[1];
    swapTiles(selectedTileClass);
}

function swapTiles(selectedTileClass) {
    // get empty tile and selected tile
    var emptyTile = document.querySelector(".t0");
    var emptyX = parseInt(emptyTile.style.left);
    var emptyY = parseInt(emptyTile.style.top);
    var selectedTile = document.querySelector("." + selectedTileClass);
    var selectedX = parseInt(selectedTile.style.left);
    var selectedY = parseInt(selectedTile.style.top);
    var tileSize = parseInt(getComputedStyle(document.body).getPropertyValue("--tilesize"), 10);
    // only swap tiles  if selected tile "next to" empty tile
    if (((Math.abs(selectedX - emptyX) === tileSize) && (selectedY === emptyY)) || ((Math.abs(selectedY - emptyY) === tileSize) && (selectedX === emptyX))) {
        if (emptyTile.style.webkitAnimationName !== 'fade') {
            emptyTile.style.webkitAnimationName = 'fade';
            emptyTile.style.webkitAnimationDuration = '.5s';
        }
        selectedTile.style.left = emptyX + "px";
        selectedTile.style.top = emptyY + "px";
        emptyTile.style.left = selectedX + "px";
        emptyTile.style.top = selectedY + "px";
        sleep(500).then(() => checkCompleteness());
    }
}

function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

function checkCompleteness() {
    complete = true;
    var tileSize = parseInt(getComputedStyle(document.body).getPropertyValue("--tilesize"), 10);
    for (var column = 0; column < 4; column++) {
        for (var row = 0; row < 4; row++) {
            var div = document.querySelector("." + idList[row * 4 + column]);
            var top = parseInt(div.style.top);
            var left = parseInt(div.style.left);
            if ((top !== row * tileSize) || (left !== column * tileSize)) {
                complete = false;
                break;
            }
        }
    }
    if (complete) {
        if (confirm("You win!\nStart over?") == true) {
            init();
        } else {
            location.href = "https://github.com/kalabalik/15tilesgame/";
        }
    }
}